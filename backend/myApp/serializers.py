from rest_framework import serializers
from myApp.models import App

class AppSerializer(serializers.ModelSerializer):
    class Meta:
        model = App
        fields = ['char_field', 'text_field', 'integer_field', 'image']