from django.db import models

class App(models.Model):
    char_field = models.CharField(max_length=255)
    text_field = models.TextField()
    integer_field = models.IntegerField()
    image = models.ImageField()

    def __str__(self):
        return self.char_field
