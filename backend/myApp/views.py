from rest_framework import viewsets
from myApp.models import App
from myApp.serializers import AppSerializer

class AppViewSet(viewsets.ModelViewSet):
    serializer_class = AppSerializer
    def get_queryset(self):
        return App.objects.all()
