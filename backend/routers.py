from rest_framework import routers
from myApp.views import AppViewSet

router = routers.DefaultRouter()
router.register(r'app', AppViewSet, basename='app')
