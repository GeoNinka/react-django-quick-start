import axios from 'axios';
import React, { useState, useEffect, useRef } from "react";


const Api = () => {
    const [responceData, setResponceData] = useState([]);
    const countRef = useRef(0);
    useEffect(() => {
        getApiData();
    }, [countRef])
    const getApiData = async () => {
        axios.get('http://localhost:8000/api/app').then((responce) => {
            setResponceData(responce.data)
            console.log(responce.data)
        }).catch((e) => {
            console.error(e);
        })
    }

    return(
        <div className="Api">
            {responceData && responceData.map((item, id) => (
                <div>
                    <p>{item.char_field}</p>
                    <p>{item.text_field}</p>
                    <p>{item.integer_field}</p>
                    <img src={item.image}/>
                </div>
            ))}
        </div>
    )
} 

export default Api;