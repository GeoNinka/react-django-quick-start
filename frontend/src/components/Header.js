import { Link } from "react-router-dom";

const Header = () => {
    return(
        <div className="header">
            <Link exact to={"/"} className="header__link">Главная</Link>
            <Link exact to={"/api"} className="header__link">Просмотр API</Link>
        </div>
    )
} 

export default Header;