import { Routes ,Route } from 'react-router-dom';

import Header from './components/Header';
import Main from './components/Main';
import Api from './components/Api';

function App() {
  return (
    <div className='App'>
      <Header></Header>
      <Routes>
        <Route exact path="/" element={<Main></Main>}/>
        <Route exact path="/api"element={<Api></Api>}/>
      </Routes>
    </div>
  );
}

export default App;
